package dragon_slayer;

//import java.io.BufferedReader;
import java.io.IOException;
//import java.io.InputStreamReader;
import java.util.Scanner;

public class World {
	static int size = 10;   
	static char[][] gameMap = new char[size][size];
	static User user1 = new User("Extrack",1,"Homme");
	static Dragon dragon1 = new Dragon("Drogon",1,"Dragon");
	static Key key1 = new Key("Osiris",-1,"Key");
	static boolean win = false;
	
	static Scanner scan = new Scanner(System.in);  
	//static BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    
	public static void main(String[] args) throws Exception, IOException 
	{
		// TODO Auto-generated method stub

		createWorld(); /// this.createWorld ???????
		//randomExit();
		printWorld();
		user1.placeCharacter(gameMap, size, 'H');
		dragon1.placeCharacter(gameMap, size, 'D');
		key1.placeCharacter(gameMap, size, 'K');
		printWorld();
		while(user1.isAlive() || win==false) {
			moveUser(user1.getPosX(),user1.getPosY());
			printWorld();
			if(win)System.out.println("WIN !");
		}
		
	    scan.close();
	    System.exit(0);
		
	}
	
	public static double getRandomIntegerBetweenRange(double min, double max){
	    double x = (int)(Math.random()*((max-min)+1))+min;
	    System.out.println("Integer between "+ min +" and "+ max+" : RandomIntegerNumber = "+x);
	    return x;
	}
	
	public static void createWorld() {
		for(int i=0;i<size;i++) {
			for(int j=0;j<size;j++) {
				if(i==0 || j==0 || i==size-1 || j==size-1) {
					gameMap[i][j]='X';
				}else if(
						(j==2||j==3||j==5)&&(i==2||i==3||i==4)
						|| 
						(j==7)&&(i==2||i==3||i==4||i==5||i==6||i==7)
						||
						(j==2||j==3)&&(i==6||i==7||i==8)
						||
						(j==5)&&(i==6||i==7)
						){
					gameMap[i][j]='X';
				}else {
					gameMap[i][j]=' ';
				}
			}
		}
		gameMap[5][size-1]='E';
	}
	
	public static void printWorld() {
		System.out.println(" ---------------------");
		for(int i=0;i<size;i++) {
			System.out.print("| ");
			for(int j=0;j<size;j++) {
				System.out.print(gameMap[i][j]+" ");
			}
			System.out.println("|");
		}
		System.out.println(" ---------------------");
	}
	
	public static void randomExit() {
		double sideRandom = getRandomIntegerBetweenRange(1,4);
		double position = getRandomIntegerBetweenRange(1,size-1);
		switch((int)sideRandom) {
			case 1:
				gameMap[0][(int)position]='E';
				break;
			case 2:
				gameMap[size-1][(int)position]='E';
				break;
			case 3:
				gameMap[(int)position][0]='E';
				break;
			case 4:
				gameMap[(int)position][size-1]='E';
				break;
		}
	}
	
	public boolean isFree(int x,int y) {
		if(gameMap[x][y]!=' ') {
			return true;
		}else {
			return false;
		}
	}
		
	public static void moveUser(int x,int y) throws NumberFormatException, IOException 
	{
	    String userInput = scan.nextLine();  	   // Read user input
	    //String userInput = input.readLine();
	    System.out.println(userInput);
	    switch(userInput) {
	    		case "z": 
	    			if(gameMap[x-1][y]==' ') {
	    				gameMap[x-1][y]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x-1, y);
	    			}else if(gameMap[x-1][y]=='K') {
	    				key1.setCatch();
	    				gameMap[x-1][y]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x-1, y);
	    			}
	    			break;
	    		case "q":
	    			if(gameMap[x][y-1]==' ') {
	    				gameMap[x][y-1]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x, y-1);
	    			}else if(gameMap[x][y-1]=='K') {
	    				key1.setCatch();
	    				gameMap[x][y-1]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x, y-1);
	    			}
	    			break;
	    		case "s":
	    			if(gameMap[x+1][y]==' ') {
	    				gameMap[x+1][y]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x+1, y);
	    			}else if(gameMap[x+1][y]=='K') {
	    				key1.setCatch();
	    				gameMap[x+1][y]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x+1, y);
	    			}
	    			break;
	    		case "d":
	    			if(gameMap[x][y+1]==' ') {
	    				gameMap[x][y+1]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x, y+1);
	    			}else if(gameMap[x][y+1]=='K') {
	    				key1.setCatch();
	    				gameMap[x][y+1]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x, y+1);
	    			}else if(gameMap[x][y+1]=='E' && key1.isCatched()) {
	    				gameMap[x][y+1]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x, y+1);
	    				win = true;
	    			}
	    			break;
	    }
	}
}
