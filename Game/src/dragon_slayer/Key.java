package dragon_slayer;

public class Key extends Character {
	
	boolean isCatched = false;

	public Key(String uName,int uLevel,String uSex){
		super(uName, uLevel, uSex);
	}
	
	public void setCatch() {
		this.isCatched = true;
	}
	
	public boolean isCatched() {
		return isCatched ? true : false;
	}
}