package dragon_slayer;

import java.io.IOException;
import java.util.Scanner;

public class Character {
	String name;
	int level;
	String sex;
	boolean alive = false;
	int x,y;
	
	public Character(String uName,int uLevel,String uSex){
		name = uName;
		level = uLevel;
		sex = uSex;
		System.out.println("User "+name+" was created with success.");
	}
	
	public void setPosition(int xpos,int ypos) {
		this.x=xpos;
		this.y=ypos;
	}
	
	public int getPosX() {
		return this.x;
	}
	
	public int getPosY() {
		return this.y;
	}
	
	public boolean isAlive() {
		if (this.alive) 
			return true;
		else 
			return false;
	}
	
	public void setLife(int x,int y) {
		this.setPosition(x,y);
		this.alive=true;
	}
	
	public void placeCharacter(char[][] gameMap,int size,char letter) {
		double x,y;
		while(!this.isAlive()) {
			x = getRandomIntegerBetweenRange(1,size-2);
			y = getRandomIntegerBetweenRange(1,size-2);
			System.out.println(letter);
			if(letter!='D') {
				if(gameMap[(int)x][(int)y]==' ') {
					this.setLife((int)x,(int)y);
					gameMap[(int)x][(int)y]=letter;
				}
			}else {
				if((gameMap[(int)x][(int)y]==' ')&&((gameMap[(int)x-1][(int)y]!='H')||(gameMap[(int)x][(int)y-1]!='H')||(gameMap[(int)x+1][(int)y]!='H')||(gameMap[(int)x][(int)y+1]!='H')) ) {
					this.setLife((int)x,(int)y);
					gameMap[(int)x][(int)y]=letter;
				}
			}
		}
	}
	
	public void kill() {
		this.alive = false;
	}
	
	public static double getRandomIntegerBetweenRange(double min, double max){
	    double x = (int)(Math.random()*((max-min)+1))+min;
	    System.out.println("Integer between "+ min +" and "+ max+" : RandomIntegerNumber = "+x);
	    return x;
	}
	
}
