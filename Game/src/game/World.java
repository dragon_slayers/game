package game;

import java.io.IOException;
import java.util.Scanner;

//import dragon_slayer.Dragon;



public class World 
{
	static int 		size 	= 10 ;
	static char[][] gameMap = new char[size][size];
	static Scanner 	sc1 	= new Scanner(System.in);
	static User 	user1 	= new User("HERO","male", 1 );
	static Dragon 	dragon1 = new Dragon();
	static Key 		key1	= new Key("lvl1_key");
	static int win = 0;
	
	public static void main (String[] args) throws java.io.IOException
	{
		createWorld();
		printWorld();
		user1.placeUser(gameMap, size);
		key1.placeKey(gameMap, size);
		dragon1.placeDragon(gameMap, size);
		placeExit(gameMap,size);
		printWorld();
		
		while(user1.isAlive()&& win==0) 
		{	
			System.out.println("win:"+win);
			moveUser(user1.getPosX(),user1.getPosY());
			System.out.println("x:"+user1.getPosX()+",Y:"+user1.getPosY());
			System.out.println("dragon - x:"+dragon1.getPosX()+",Y:"+dragon1.getPosY());
			isKilled();
			printWorld();
			if(win==1)System.out.println("WIN !");
			if(win==-1)System.out.println("LOOSE!");
		}
		
	   sc1.close();
	   System.exit(0);
		
		
	}

	public static double getRandomIntegerBetweenRange(double min, double max)
	{
	    double x = (int)(Math.random()*((max-min)+1))+min;
	    System.out.println("Integer between "+ min +" and "+ max+" : RandomIntegerNumber = "+x);
	    return x;
	}
	
	
	public static void createWorld() 
	{
		for (int i = 0; i < size; i++) 
		{
			for (int j = 0; j < size; j++) 
			{
				if(i==0 || j==0 || i==size-1 || j==size-1) 
				{
					gameMap[i][j]='X';
				}
				else if (
				(j==2||j==3||j==5)&&(i==2||i==3||i==4)
				|| 
				(j==7)&&(i==2||i==3||i==4||i==5||i==6||i==7)
				||
				(j==2||j==3)&&(i==6||i==7||i==8)
				||
				(j==5)&&(i==6||i==7)
				) 
				{
					gameMap[i][j]='X';
				}
				else
				{
					gameMap[i][j]=' ';		
				}
			}
		}
		//gameMap[size/2-1][size-1]='E';
	}
	public static void printWorld()
	{
		for (int i = 0; i < 2*size+1; i++) {
			System.out.print("#");
		}
		System.out.println("#");
		for(int i=0;i<size;i++) 
		{
			System.out.print("#");
			for(int j=0;j<size;j++) 
			{
				System.out.print(gameMap[i][j]+" ");
			}
			System.out.println("#");
		}
		for (int k = 0; k <2*size+1; k++) 
		{
			System.out.print("#");
		}
		System.out.println("#");	
	}
	
	public static void moveUser(int x,int y) throws NumberFormatException, IOException 
	{
	    String userInput = sc1.nextLine();  	   // Read user input
	    //String userInput = input.readLine();
	    System.out.println(userInput);
	    switch(userInput) {
	    		case ("w"): 
	    			if(gameMap[x-1][y]==' ') 
	    			{
	    				System.out.println("A");
	    				gameMap[x-1][y]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x-1, y);
	    			}
	    			else if(gameMap[x-1][y]=='K')
	    			{
	    				key1.setCatch();
	    				gameMap[x-1][y]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x-1, y);
	    			}
	    			else if(gameMap[x-1][y]=='E' && key1.isCatched()) 
	    			{
	    				gameMap[x-1][y]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x-1, y);
	    				win = 1;
	    			}
	    			break;
	    		case "a":
	    			if(gameMap[x][y-1]==' ') 
	    			{
	    				System.out.println("S");
	    				gameMap[x][y-1]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x, y-1);
	    			}
	    			else if(gameMap[x][y-1]=='K') 
	    			{
	    			key1.setCatch();
	    				gameMap[x][y-1]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x, y-1);
	    			}
	    			else if(gameMap[x][y-1]=='E' && key1.isCatched()) 
	    			{
	    				gameMap[x][y-1]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x, y-1);
	    				win = 1;
	    			}
	    			break;
	    		case "s":
	    			if(gameMap[x+1][y]==' ') 
	    			{
	    				System.out.println("D");
	    				gameMap[x+1][y]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x+1, y);
	    			}
	    			else if(gameMap[x+1][y]=='K') 
	    			{
	    				key1.setCatch();
	    				gameMap[x+1][y]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x+1, y);
	    			}
	    			else if(gameMap[x+1][y]=='E' && key1.isCatched()) 
	    			{
	    				gameMap[x+1][y]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x+1, y);
	    				win = 1;
	    			}
	    			break;
	    		case "d":
	    			if(gameMap[x][y+1]==' ') 
	    			{
	    				System.out.println("w");
	    				gameMap[x][y+1]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x, y+1);
	    			}
	    			else if(gameMap[x][y+1]=='K') 
	    			{
	    				key1.setCatch();
	    				gameMap[x][y+1]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x, y+1);
	    			}
	    			else if(gameMap[x][y+1]=='E' && key1.isCatched()) 
	    			{
	    				gameMap[x][y+1]='H';
	    				gameMap[x][y]=' ';
	    				user1.setPosition(x, y+1);
	    				win = 1;
	    			}
	    			break;
	    }
	}
	public static void isKilled() 
	{
		
		if((dragon1.getPosX()-user1.getPosX()==1 ||dragon1.getPosX()-user1.getPosX()==-1)&& dragon1.getPosY()==user1.getPosY() )
			{
				user1.kill();
				win=-1;
			}
		if((dragon1.getPosY()-user1.getPosY()==1 ||dragon1.getPosY()-user1.getPosY()==-1)&& dragon1.getPosX()==user1.getPosX() )
			{
				
				user1.kill();
				win=-1;
			}
	}
	
	public static void placeExit(char[][] gameMap, int size) 
	{
		int x=0, y=0, side;
		side=(int) getRandomIntegerBetweenRange((double)0,(double) 3);
		System.out.println("side:"+side);
		switch (side)
		{
		case 0:
			do 
			{
			x =(int)getRandomIntegerBetweenRange((double)1,(double) size-2);
			y=0;
			}while(isCloseY(x,y,gameMap,size));
			break;
			
		case 1:
			do
			{
			x =(int)getRandomIntegerBetweenRange((double)1,(double) size-2);
			y=size-1;
			}while(isCloseY(x,y,gameMap,size));
			break;
		case 2:
			do
			{
			y =(int)getRandomIntegerBetweenRange((double)1,(double) size-2);
			x=0;
			}while(isCloseX(x,y,gameMap,size));
			break;
		case 3:
			do {
			y =(int)getRandomIntegerBetweenRange((double)1,(double) size-2);
			x=size-1;
			}while(isCloseX(x,y,gameMap,size));
			break;
		default:
			System.out.println("#adshoisauhioudasoasidy");
			break;
		}
		gameMap[x][y]='E';
	}
	public static boolean isCloseX(int x,int y,char[][]gameMap,int size) 
	{
		/*if(user1.getPosX()==(x-1)||user1.getPosX()==(x+1)||dragon1.getPosX()==(x-1)||
				dragon1.getPosX()==(x+1)||key1.getPosX()==(x-1)||key1.getPosX()==(x+1))*/
		if (x==0) 
		{
			if(gameMap[x+1][y]==' ')
			{
				return false;
			}
			else
			{
				return true;
			}
			
		}
		else if (x==size-1) 
		{
			if(gameMap[x-1][y]==' ')
			{
				return false;
			}
			else
			{
				return true;
			}
			
			
		}
		else
		{
			System.out.println("#xxxx");
			return true;	
		}
		
	}
	public static boolean isCloseY(int x,int y,char[][]gameMap,int size) 
	{
		/*if(user1.getPosY()==(y-1)||user1.getPosY()==(y+1)||dragon1.getPosY()==(y-1)||
				dragon1.getPosY()==(y+1)||key1.getPosY()==(y-1)||key1.getPosY()==(y+1))*/
		if (y==0) 
		{
			if(gameMap[x][y+1]==' ')
			{
				return false;
			}
			else
			{
				return true;
			}
			
		}
		if (y==size-1) 
		{
			if(gameMap[x][y-1]==' ')
			{
				return false;
			}
			else
			{
				return true;
			}
			
			
		}
		else
		{
			System.out.println("#yyyyyyy");
			return true;	
		}
	}
}
