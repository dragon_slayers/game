package game;

public class Dragon extends Character 
{	
	public boolean isPlaced = false;
	public Dragon()
	{
		super();
	}
	public Dragon(String dName)
	{
		super(dName);
	}
	public Dragon(String dName, int dLevel)
	{
		super(dName,dLevel);
	}
	
	public void placeDragon(char[][] gameMap, int size)
	{
		int x=0,y=0;
		while(!isPlaced)
		{
			x =(int)getRandomIntegerBetweenRange((double)1,(double) size-2);
			y =(int)getRandomIntegerBetweenRange((double)1,(double) size-2);
			System.out.println("XININ:"+x +"YINI"+y);
			if(		(gameMap[x][y]==' ')&&((gameMap[x-1][y]!='H')||
					(gameMap[x][y-1]!='H')||(gameMap[x+1][y]!='H')||
					(gameMap[x][y+1]!='H')) 
			   ) 
			{
				isPlaced = true;
				setPosition(x, y);
				gameMap[x][y]='D';
				break;
			}
		}
	}
}