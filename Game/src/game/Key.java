package game;

public class Key extends Character
{
	boolean isCatched = false;
	public boolean isPlaced = false;
	public Key(String kName)
	{
		super(kName);
	}
	public Key(String kName, int kLevel)
	{
		super(kName,kLevel);
	}
	public void setCatch()
	{
		this.isCatched = true;
	}
	public boolean isCatched() 
	{
		return isCatched ? true : false;
	}
	
	public void placeKey(char[][] gameMap, int size)
	{
		int x=0,y=0;
		while(!isPlaced)
		{
			x =(int)getRandomIntegerBetweenRange((double)1,(double) size-2);
			y =(int)getRandomIntegerBetweenRange((double)1,(double) size-2);
			System.out.println("Key: XININ:"+x +"YINI: "+y);
			if(gameMap[x][y]==' ')
			{
				isPlaced = true;
				setPosition(x, y);
				gameMap[x][y]='K';
				break;
			}
		}
	}
}
