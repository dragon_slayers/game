package game;

public class Drogon extends Character 
{	
	public boolean isPlaced = false;
	public Drogon()
	{
		super();
	}
	public Drogon(String dName)
	{
		super(dName);
	}
	public Drogon(String dName, int dLevel)
	{
		super(dName,dLevel);
	}
	
	
	
	public void placeDrogon(char[][] gameMap, int size)
	{
		int x=0,y=0;
		while(!isPlaced)
		{
			x =(int)getRandomIntegerBetweenRange((double)1,(double) size-2);
			y =(int)getRandomIntegerBetweenRange((double)1,(double) size-2);
			System.out.println("Dragon: XININ:"+x +"YINI:"+y);
			if(		(gameMap[x][y]==' ')&&((gameMap[x-1][y]!='H')||
					(gameMap[x][y-1]!='H')||(gameMap[x+1][y]!='H')||
					(gameMap[x][y+1]!='H')) 
			   ) 
			{
				isPlaced = true;
				setPosition(x, y);
				gameMap[x][y]='D';
				break;
			}
		}
	}
}
