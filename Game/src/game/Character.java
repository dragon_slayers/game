package game;


public class Character 
{	
	String name;
	int x,y;
	boolean alive = false;
	int level;
	
	public Character()
	{	
	}
	public Character(String Name)
	{
		name = Name;
		System.out.println("Character with "+name+" was created with success.");
	}
	
	public Character(String Name, int Level)
	{
		level = Level;
		name = Name;
		System.out.println("Character with "+name+" was created with success in level"+level);
	}
	
	public void setPosition(int xpos,int ypos) 
	{
		this.x=xpos;
		this.y=ypos;
	}
	
	public int getPosX() 
	{
		return this.x;
	}
	
	public int getPosY() 
	{
		return this.y;
	}
			
	public static double getRandomIntegerBetweenRange(double min, double max)
	{
	    double x = (int)(Math.random()*((max-min)+1))+min;
	    System.out.println("Integer between "+ min +" and "+ max+" : RandomIntegerNumber = "+x);
	    return x;
	}
	



}
